#!/bin/bash

SOURCE_LOCATION=$1
OUTPUT_DIR=$2
RESOURCE_NAME=$3
FILE_FILTER=$4

echo $1
echo $2
echo $3
echo $4

cat << EOF > "${OUTPUT_DIR}/${RESOURCE_NAME}.h"
#include <iostream>
#include <vector>

extern std::vector<std::string> ${RESOURCE_NAME};

EOF

RESOURCE_FILES=$(echo ${FILE_FILTER} | sed 's/;/\",\"/g' );
echo ${RESOURCE_FILES}

cat << EOF > "${OUTPUT_DIR}/${RESOURCE_NAME}.cpp"
#include "${RESOURCE_NAME}.h"

std::vector<std::string> ${RESOURCE_NAME} = {"${RESOURCE_FILES}"};

EOF

