#include <iostream>
#include <vector>

#include <Resources.h>

int main(int argc, char const *argv[])
{
    for(auto r : Resources)
        std::cout << r << std::endl;
    return 0;
}
